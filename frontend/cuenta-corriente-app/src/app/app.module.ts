import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CuentaCorrienteComponent, ModalsEliminarCtaCte } from './component/cuenta-corriente/cuenta-corriente.component';

import { MaterialModule } from './material/material.module';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { CuentaCorrienteService } from './service/cuenta-corriente-service.service';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorIntl, MatPaginatorModule } from '@angular/material/paginator';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { ModalsAddMovimiento, ModalsVerDetalleMovimiento, MovimientosComponent } from './component/movimientos/movimientos.component';
import { AddCuentaCorrienteComponent } from './component/add-cuenta-corriente/add-cuenta-corriente.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { getSpanishPaginatorIntl } from './spanish-mat-paginator';
import { TwoDigitDecimaNumberDirective } from './twoDigitDecimaNumberDirective';
import { MatInputModule } from '@angular/material/input';
import { MatProgressBarModule } from '@angular/material/progress-bar';


@NgModule({
  declarations: [
    AppComponent,
    CuentaCorrienteComponent,
    AddCuentaCorrienteComponent,
    ModalsEliminarCtaCte,
    MovimientosComponent,
    ModalsAddMovimiento,
    ModalsVerDetalleMovimiento,
    TwoDigitDecimaNumberDirective
  ],
  imports: [
    BrowserModule,
    MatTableModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatSelectModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MaterialModule,
    MatPaginatorModule,
    MatSnackBarModule,
    MatDialogModule,
    MatButtonModule,
    MatProgressBarModule,

    RouterModule.forRoot([
      { path: '', component: CuentaCorrienteComponent },
      { path: 'cuentaCorriente', component: CuentaCorrienteComponent },
      { path: 'cuentaCorriente/add', component: AddCuentaCorrienteComponent },
      { path: 'cuentaCorriente/:id', component: MovimientosComponent },
    ],
      {
        onSameUrlNavigation: "reload"
      })
  ],

  entryComponents: [
    ModalsEliminarCtaCte,
    ModalsVerDetalleMovimiento,
    ModalsAddMovimiento
  ],
  providers: [
    CuentaCorrienteService,
    { provide: MatPaginatorIntl, useValue: getSpanishPaginatorIntl() }
  ],
  exports:[
      BrowserModule,
      FormsModule,
      ReactiveFormsModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
