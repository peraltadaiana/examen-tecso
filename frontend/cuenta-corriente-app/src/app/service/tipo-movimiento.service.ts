import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { TipoMovimientoDto } from '../dto/TipoMovimientoDto';

@Injectable({
  providedIn: 'root'
})
export class TipoMovimientoService {

  constructor(private http : HttpClient) { }
/**
 * Devuelve los tipos de movimientos existentes (DEBITO O CREDITO)
 */
  public getAllTipoMovimiento(): Observable<TipoMovimientoDto[]> {
    return this.http.get<TipoMovimientoDto[]>(`${environment.url}tipomovimiento`);
  }
}
