import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

import { CuentaCorrienteDto } from '../dto/CuentaCorrienteDto';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { CuentaCorrienteInputDto } from '../dto/CuentaCorrienteInputDto';
import { MovimientoDto } from '../dto/MovimientoDto';
import { MovimientoInputDto } from '../dto/MovimientoInputDto';

@Injectable({
  providedIn: 'root'
})
export class CuentaCorrienteService {

  constructor(private http: HttpClient) { }
  /**
   * devuelve el listado de todas las cuentas corrientes
   */
  public getAll(): Observable<CuentaCorrienteDto[]> {
    return this.http.get<CuentaCorrienteDto[]>(`${environment.url}cuentacorriente`);
  }

  /**
   * Solicita una cuenta corriente po id cuenta corriente
   * @param id id de la cuenta corriente
   */
  public getCtaCte(id: number): Observable<CuentaCorrienteDto> {
    return this.http.get<CuentaCorrienteDto>(`${environment.url}cuentacorriente/${id}`);
  }

  /**
   * Elimina una cuenta corriente
   * @param id id cuenta corriente
   */
  public deleteCtaCte(id: number) {
    return this.http.delete(`${environment.url}cuentacorriente/${id}`);
  }
  /**
   * Agrega una nueva cuenta corriente
   * @param cuentaCorrienteInputDto 
   */

  public agregarCuentaCorriente(cuentaCorrienteInputDto: CuentaCorrienteInputDto){
    return this.http.post(`${environment.url}cuentacorriente`, cuentaCorrienteInputDto);
  }
  /**
   * Obtiene todos los movimientos de una cuenta corriente
   * @param id id cuenta corriente
   */
  public getMovimientos(id: number): Observable<MovimientoDto[]> {
    return this.http.get<MovimientoDto[]>(`${environment.url}cuentacorriente/${id}/movimientos`);
  }
  /**
   * Agrega un movimiento a una cuenta corriente
   * @param movimientiInputDto 
   * @param id id de la cuenta corriente
   */
  public agregarMovimiento(movimientiInputDto: MovimientoInputDto, id: number){
    return this.http.post(`${environment.url}cuentacorriente/${id}/movimiento`,movimientiInputDto)
  }

}
