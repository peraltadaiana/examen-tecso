import { TestBed } from '@angular/core/testing';

import { CuentaCorrienteService } from './cuenta-corriente-service.service';

describe('CuentaCorrienteServiceService', () => {

  let service: CuentaCorrienteService;



  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CuentaCorrienteService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
