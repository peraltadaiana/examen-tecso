import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { MonedaDto } from '../dto/MonedaDto';

@Injectable({
  providedIn: 'root'
})
export class MonedaService {

  constructor(private http: HttpClient) { }
  /**
   * Devuelve la lsita de todas las monedas existentes
   */
  public getAllMoneda(): Observable<MonedaDto[]> {
    return this.http.get<MonedaDto[]>(`${environment.url}moneda`);
  }
}
