export class MovimientoDto {
    public id: number = 0;
    public fecha: Date = new Date();
    public descripcionTipoMovimiento: string = "";
    public importe: number = 0;
    public descripcion: string = "";
}