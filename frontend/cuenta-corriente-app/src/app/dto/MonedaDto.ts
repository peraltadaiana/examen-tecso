export class MonedaDto{
    public id : number = 0;
	public descripcion : string = '';
	public codigo : string = '';
	public montoMaximoDescubierto : number = 0; 
}