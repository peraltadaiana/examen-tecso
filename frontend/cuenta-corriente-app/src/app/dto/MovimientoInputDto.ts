export class MovimientoInputDto{

	public tipoMovimientoId : number = 0;
	public descripcion : string = "";
	public importe : number = 0;
}