import { MonedaDto } from "./MonedaDto";

export class CuentaCorrienteDto {

    public id : number = 0;
	public numeroCuenta : string = '';
	public moneda : MonedaDto = new MonedaDto();
    public saldo :number = 0;
}