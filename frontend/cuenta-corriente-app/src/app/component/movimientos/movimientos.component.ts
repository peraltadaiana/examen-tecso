import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';



import { CuentaCorrienteDto } from 'src/app/dto/CuentaCorrienteDto';
import { MovimientoDto } from 'src/app/dto/MovimientoDto';
import { MovimientoInputDto } from 'src/app/dto/MovimientoInputDto';
import { TipoMovimientoDto } from 'src/app/dto/TipoMovimientoDto';
import { CuentaCorrienteService } from 'src/app/service/cuenta-corriente-service.service';
import { TipoMovimientoService } from 'src/app/service/tipo-movimiento.service';

@Component({
  selector: 'app-movimientos',
  templateUrl: './movimientos.component.html',
  styleUrls: ['./movimientos.component.css']
})
export class MovimientosComponent implements OnInit {

  public cuentaCorrienteId: number = 0;
  public cargando: boolean = true;
  public cuentaCorrienteDto: CuentaCorrienteDto = new CuentaCorrienteDto();
  public movimientosList: MovimientoDto[] = [];
  public columnMovimientos: string[] = ['tipoMovimiento', 'fecha', 'importe', 'descripcion'];

  public dataMovimientos: MatTableDataSource<MovimientoDto> = new MatTableDataSource<MovimientoDto>([])


  constructor(private route: ActivatedRoute,
    public dialog: MatDialog,
    private ctaCteService: CuentaCorrienteService) {
    this.cargando = true;
    this.route.paramMap.subscribe(params => {

      this.cuentaCorrienteId = parseInt(params.get("id")!);

      if (!isNaN(this.cuentaCorrienteId)) {
        this.cargarMovimientos();
        this.cargando = false;
      }
    });
  }

  private cargarMovimientos() {
    this.ctaCteService.getCtaCte(this.cuentaCorrienteId).subscribe(ctacte => {
      this.cuentaCorrienteDto = ctacte;
      this.ctaCteService.getMovimientos(this.cuentaCorrienteId).subscribe((movimientosList) => {
        this.movimientosList = movimientosList;
        this.dataMovimientos = new MatTableDataSource<MovimientoDto>(movimientosList);
      });
    });
  }

  ngOnInit(): void {
    this.dataMovimientos = new MatTableDataSource<MovimientoDto>([]);
  }

  @ViewChild(MatPaginator, { static: false }) set matPaginator(paginator: MatPaginator) {
    this.dataMovimientos.paginator = paginator;
  }

  openDialogDetalle(descripcion: string): void {
    const dialogRef = this.dialog.open(ModalsVerDetalleMovimiento, {
      data: <DialogVerDetalle>{
        descripcion: descripcion
      }
    });


  }

  openDialogAgregarMovimiento(): void {
    const dialogAddMovimiento = this.dialog.open(ModalsAddMovimiento, {
      data: <DialogAddMovimiento>{
        id: this.cuentaCorrienteId,
        codigoMoneda: this.cuentaCorrienteDto.moneda.codigo
      },
      width: '500px'
    }).afterClosed().subscribe(() => {
      this.cargando = true;
      this.cargarMovimientos();
      this.cargando = false;
    });
  }
}

export interface DialogVerDetalle {
  descripcion: string
}

export interface DialogAddMovimiento {
  id: number;
  codigoMoneda: string;
}

@Component({
  selector: 'modals-ver-detalle-movimiento',
  templateUrl: 'modals-ver-detalle-movimiento.html',
})
export class ModalsVerDetalleMovimiento {

  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogVerDetalle,
    public dialogRef: MatDialogRef<ModalsVerDetalleMovimiento>) { }


  onNoClick(): void {
    this.dialogRef.close();
  }

}


@Component({
  selector: 'modals-add-movimiento',
  templateUrl: 'modals-add-movimiento.html',
  styleUrls: ['movimientos.component.css']
})
export class ModalsAddMovimiento {

  public tipoMovimientoList: TipoMovimientoDto[] = [];
  public movimientoInputDto: MovimientoInputDto = new MovimientoInputDto();
  //formulario del modals
  public movimientoForm: FormGroup = new FormGroup({
    importe: new FormControl(''),
    tipoMovimientoId: new FormControl(''),
    descripcion: new FormControl('')
  });


  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogAddMovimiento,
    public dialogAddMovimiento: MatDialogRef<ModalsAddMovimiento>,
    public ctaCteService: CuentaCorrienteService,
    public tipoMovimientoService: TipoMovimientoService,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar) {
    this.getAllTipoMovimiento();
  }

  ngOnInit(): void {
    this.movimientoForm = this.formBuilder.group({
      importe: new FormControl('', [Validators.required, Validators.min(0), Validators.pattern('^[0-9]\\d*(\\.\\d{1,2})?$')]),
      tipoMovimientoId: new FormControl('', [Validators.required]),
      descripcion: new FormControl('', [Validators.maxLength(200)])
    })
  }

  public hasError = (controlName: string, errorName: string) => {
    if (this.movimientoForm) {
      return this.movimientoForm.controls[controlName].hasError(errorName);
    }
    return false;
  }

  getAllTipoMovimiento() {
    this.tipoMovimientoService.getAllTipoMovimiento().subscribe(tipoMovimientoList => {
      this.tipoMovimientoList = tipoMovimientoList;

    })
  }

  onNoClick(): void {
    this.dialogAddMovimiento.close();
  }


  agregarMovimiento(id: number) {

    this.movimientoInputDto.importe = this.movimientoForm.get('importe')?.value;
    this.movimientoInputDto.tipoMovimientoId = this.movimientoForm.get('tipoMovimientoId')?.value;
    this.movimientoInputDto.descripcion = this.movimientoForm.get('descripcion')?.value;
    this.ctaCteService.agregarMovimiento(this.movimientoInputDto, id).subscribe(() => {

      this.onNoClick();
      this.openSnackBar("Movimiento realizado con éxito.", '', true);
    },
      error => {
        if (parseInt(error.status) < 500) {
          this.openSnackBar(error.error, '', false);
        }
        else {
          this.openSnackBar('Error inesperado.', '', false);
        }
      })


  }

  openSnackBar(message: string, action: string, exito: boolean) {
    this.snackBar.open(message, action, {
      duration: 3000,
      panelClass: [exito ? 'exito-snackbar' : 'error-snackbar']
    });
  }


}
