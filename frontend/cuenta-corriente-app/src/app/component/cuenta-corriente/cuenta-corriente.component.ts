import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { CuentaCorrienteDto } from 'src/app/dto/CuentaCorrienteDto';
import { CuentaCorrienteService } from 'src/app/service/cuenta-corriente-service.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';



@Component({
  selector: 'app-cuenta-corriente',
  templateUrl: './cuenta-corriente.component.html',
  styleUrls: ['./cuenta-corriente.component.css']
})
export class CuentaCorrienteComponent implements OnInit {

  public cuentaCorrienteList: CuentaCorrienteDto[] = [];

  public cargando: boolean = true;

  public columnCtaCte: string[] = ['numeroCuenta', 'descripcionMoneda', 'saldo', 'eliminar'];

  public dataCtaCte: MatTableDataSource<CuentaCorrienteDto> = new MatTableDataSource<CuentaCorrienteDto>([])



  constructor(private ctaCteService: CuentaCorrienteService,
    public dialog: MatDialog) {
    this.cargando = true;
    debugger;
    this.getAllCtaCte();
    this.cargando = false;
  }


  ngOnInit(): void {
    this.dataCtaCte = new MatTableDataSource<CuentaCorrienteDto>([]);
  }

  @ViewChild(MatPaginator, { static: false }) set matPaginator(paginator: MatPaginator) {
    this.dataCtaCte.paginator = paginator;
  }

  getAllCtaCte() {
    this.cargando = true;
    this.ctaCteService.getAll().subscribe(data => {
      this.cuentaCorrienteList = data;
      this.dataCtaCte = new MatTableDataSource<CuentaCorrienteDto>(data);
      this.cargando = false;
    })
  }

  openDialogEliminar(id: number, numeroCuenta: string): void {
    const dialogRef = this.dialog.open(ModalsEliminarCtaCte, {
      data: <DialogEliminar>{
        id: id,
        numeroCuenta: numeroCuenta
      }
    });

    dialogRef.afterClosed().subscribe(() => {
      this.getAllCtaCte();
    })
  }

}



export interface DialogEliminar {
  id: number;
  numeroCuenta: string
}


@Component({
  selector: 'modals-eliminar-ctacte',
  templateUrl: 'modals-eliminar-ctacte.html',
})
export class ModalsEliminarCtaCte {

  constructor(
    private ctaCteService: CuentaCorrienteService,
    private snackBar: MatSnackBar,
    public dialogRef: MatDialogRef<ModalsEliminarCtaCte>,
    @Inject(MAT_DIALOG_DATA) public data: DialogEliminar) { }


  onNoClick(): void {
    this.dialogRef.close();
  }

  eliminar(id: number) {

    this.ctaCteService.deleteCtaCte(id).subscribe(() => {
      this.dialogRef.close();
      this.openSnackBar("La cuenta corriente se eliminó correctamente.", '', true);
    },
      error => {
        if (parseInt(error.status) < 500) {
          this.dialogRef.close();
          this.openSnackBar(error.error, '', false);
        }
        else {
          this.dialogRef.close();
          this.openSnackBar('Error inesperado.', '', false);
        }
      }
    )


  }

  openSnackBar(message: string, action: string, exito: boolean) {
    this.snackBar.open(message, action, {
      duration: 3000,
      panelClass: [exito ? 'exito-snackbar' : 'error-snackbar']
    });
  }
}
