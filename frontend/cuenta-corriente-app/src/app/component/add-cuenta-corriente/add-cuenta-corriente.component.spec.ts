import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCuentaCorrienteComponent } from './add-cuenta-corriente.component';

describe('AddCuentaCorrienteComponent', () => {
  let component: AddCuentaCorrienteComponent;
  let fixture: ComponentFixture<AddCuentaCorrienteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddCuentaCorrienteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCuentaCorrienteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
