import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CuentaCorrienteInputDto } from 'src/app/dto/CuentaCorrienteInputDto';
import { MonedaDto } from 'src/app/dto/MonedaDto';
import { CuentaCorrienteService } from 'src/app/service/cuenta-corriente-service.service';
import { MonedaService } from 'src/app/service/moneda.service';

@Component({
  selector: 'app-add-cuenta-corriente',
  templateUrl: './add-cuenta-corriente.component.html',
  styleUrls: ['./add-cuenta-corriente.component.css']
})
export class AddCuentaCorrienteComponent implements OnInit {

  private cuentaCorrienteInputDto: CuentaCorrienteInputDto = new CuentaCorrienteInputDto();

  public cuentaCorrienteForm: FormGroup = new FormGroup({
    numeroCuenta: new FormControl(''),
    monedaId: new FormControl('')
  });

  public monedas: MonedaDto[] = [];


  constructor(private ctaCteService: CuentaCorrienteService,
    private formBuilder: FormBuilder,
    private location: Location,
    private monedaService: MonedaService,
    private snackBar: MatSnackBar) {
    this.getAllMoneda();
  }

  ngOnInit(): void {
    this.cuentaCorrienteForm = this.formBuilder.group({
      numeroCuenta: new FormControl('', [Validators.required]),
      monedaId: new FormControl('', [Validators.required])

    })

    //this.cuentaCorrienteForm.controls.numeroCuenta
  }


  getAllMoneda() {
    this.monedaService.getAllMoneda().subscribe(monedas => {
      this.monedas = monedas;

    })
  }

  public hasError = (controlName: string, errorName: string) => {
    if (this.cuentaCorrienteForm) {
      return this.cuentaCorrienteForm.controls[controlName].hasError(errorName);
    }
    return false;
  }

  addCuentaCorriente() {
    this.cuentaCorrienteInputDto.monedaId = this.cuentaCorrienteForm.get('monedaId')?.value;
    this.cuentaCorrienteInputDto.numeroCuenta = this.cuentaCorrienteForm.get('numeroCuenta')?.value;
    this.ctaCteService.agregarCuentaCorriente(this.cuentaCorrienteInputDto).subscribe(() => {
      debugger;
      this.goBack();
      this.openSnackBar("La cuenta corriente se agregó correctamente.", '', true);
    },
      error => {
        if (parseInt(error.status) < 500) {
          this.openSnackBar(error.error, '', false);
        }
        else {
          this.openSnackBar('Error inesperado.', '', false);
        }
      })
  }

  openSnackBar(message: string, action: string, exito: boolean) {
    this.snackBar.open(message, action, {
      duration: 3000,
      panelClass: [exito ? 'exito-snackbar' : 'error-snackbar']
    });
  }

  goBack(): void {
    return this.location.back();
  }


}
