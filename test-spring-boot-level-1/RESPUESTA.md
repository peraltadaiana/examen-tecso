# Examen Tecso Daiana Peralta

Descargando el repositorio:

	- git clone "https://gitlab.com/peraltadaiana/examen-tecso.git"

Para ejecutar la aplicacion utilizando Maven:

	mvn spring-boot:run -DjvmArgs="-Dspring.profiles.active=dev"

Probando controllers:
	
	CREAR CUENTA CORRIENTE:		
		curl -X POST -H "Content-Type: application/json" -d "{ \"numeroCuenta\": \"CC87685848\",  \"monedaId\": 1  }" http://localhost:8080/api/cuentacorriente

	ELIMINAR UNA CUENTA CORRIENTE:
		curl -X DELETE localhost:8080/api/cuentacorriente/2
	

	LISTAR TODAS LAS CUENTAS CORRIENTES:
		curl -X GET localhost:8080/api/cuentacorriente

	AGREGAR MOVIMIENTO:
		curl -X POST -H "Content-Type: application/json" -d "{ \"descripcion\": \"Transferencia inmediata\",  \"tipoMovimientoId\": 2 , \"importe\": 200  }" localhost:8080/api/cuentacorriente/1/movimiento

	LISTAR MOVIMIENTOS POR CUENTA:
		curl -X GET localhost:8080/api/cuentacorriente/1/movimientos

Para ejecutar el frontend:
	npm install
	ng serve






