package coop.tecso.examen.model;
import javax.persistence.Entity;


@Entity
public class TipoMovimiento extends AbstractPersistentObject{

	private static final long serialVersionUID = 1L;
	
	private String descripcion;
	private Integer signo;

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getSigno() {
		return signo;
	}

	public void setSigno(Integer signo) {
		this.signo = signo;
	}
		
}
