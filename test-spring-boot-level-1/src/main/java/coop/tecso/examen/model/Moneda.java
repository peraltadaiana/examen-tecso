package coop.tecso.examen.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Moneda extends AbstractPersistentObject {

	private static final long serialVersionUID = 1L;

	private String descripcion;
	private BigDecimal montoMaximoDescubierto;

	@Column(unique = true)
	private String codigo;

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public BigDecimal getMontoMaximoDescubierto() {
		return montoMaximoDescubierto;
	}

	public void setMontoMaximoDescubierto(BigDecimal montoMaximoDescubierto) {
		this.montoMaximoDescubierto = montoMaximoDescubierto;
	}

}
