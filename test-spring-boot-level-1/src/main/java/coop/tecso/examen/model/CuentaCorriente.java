package coop.tecso.examen.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Formula;

import coop.tecso.examen.dto.MovimientoInputDto;
import coop.tecso.examen.exception.DescubiertoExcedidoException;

@Entity
@DynamicUpdate(true)
public class CuentaCorriente extends AbstractPersistentObject {

	private static final long serialVersionUID = 1L;

	@ManyToOne
	private Moneda moneda;

	@Column(unique = true)
	private String numeroCuenta;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "cuentaCorriente")
	private Set<Movimiento> movimientoList;

	@Formula("SELECT sum(m.importe * tm.signo) FROM movimiento m , tipo_movimiento tm"
			+ " WHERE m.tipo_movimiento_id = tm.id AND m.cuenta_corriente_id = id")
	private BigDecimal saldo;

	public Moneda getMoneda() {
		return moneda;
	}

	public void setMoneda(Moneda moneda) {
		this.moneda = moneda;
	}

	public String getNumeroCuenta() {
		return numeroCuenta;
	}

	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}

	public Set<Movimiento> getMovimientoList() {
		return movimientoList;
	}

	public void setMovimientoList(Set<Movimiento> movimientoList) {
		this.movimientoList = movimientoList;
	}

	public BigDecimal getSaldo() {
		return saldo;
	}

	protected void setSaldo(BigDecimal saldo) {
		this.saldo = saldo;
	}

	public void agregarMovimiento(MovimientoInputDto dto, TipoMovimiento tipoMovimiento)
			throws DescubiertoExcedidoException {

		if ((this.getSaldoBD().doubleValue() + (dto.getImporte().doubleValue() * tipoMovimiento.getSigno())) >= -(this
				.getMoneda().getMontoMaximoDescubierto().doubleValue())) {
			Movimiento movimiento = new Movimiento();
			movimiento.setDescripcion(dto.getDescripcion());
			movimiento.setCuentaCorriente(this);
			movimiento.setFecha(new Date());
			movimiento.setImporte((dto.getImporte()).setScale(2, BigDecimal.ROUND_DOWN));

			movimiento.setTipoMovimiento(tipoMovimiento);
			this.movimientoList.add(movimiento);
		} else {
			// El importe del movimiento supera el limite de descubierto
			throw new DescubiertoExcedidoException(this.numeroCuenta,
					new BigDecimal((this.getSaldoBD().doubleValue()
							+ (dto.getImporte().doubleValue() * tipoMovimiento.getSigno())) * -1),
					this.getMoneda().getMontoMaximoDescubierto());
		}

	}

	/**
	 * Devuelve el saldo de la cuenta o cero en caso de que la cuenta no tenga
	 * movimintos.
	 * 
	 * @return
	 */
	public BigDecimal getSaldoBD() {
		BigDecimal saldo = new BigDecimal(0);
		// REMARKS: dado que al mapear el saldo como Formula, en caso de no tener
		// resultados la formula devuelve null,
		// contemplo el caso y devuelvo cero para poder manejarlo mas
		// facilmente en el codigo que usa el saldo.
		if (this.getSaldo() != null) {
			saldo = this.saldo;
		}
		return saldo;
	}

}
