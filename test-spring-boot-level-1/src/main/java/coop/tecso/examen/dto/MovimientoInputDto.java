package coop.tecso.examen.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class MovimientoInputDto implements Serializable {

	private static final long serialVersionUID = 1L;

	@NotNull(message = "El tipo de movimiento no puede ser nulo")
	private Long tipoMovimientoId;

	@Size(max = 200, message = "La descripcion del movimiento no puede contener mas de 200 caracteres.")
	private String descripcion;

	@NotNull(message = "El importe del movimiento no puede ser nulo.")
	private BigDecimal importe;

	public Long getTipoMovimientoId() {
		return tipoMovimientoId;
	}

	public void setTipoMovimientoId(Long tipoMovimientoId) {
		this.tipoMovimientoId = tipoMovimientoId;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public BigDecimal getImporte() {
		return importe;
	}

	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}

}
