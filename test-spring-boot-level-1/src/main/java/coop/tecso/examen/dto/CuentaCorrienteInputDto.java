package coop.tecso.examen.dto;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class CuentaCorrienteInputDto implements Serializable {

	private static final long serialVersionUID = 1L;

	@NotBlank(message = "El numero de cuenta no puede ser nulo o vacio.")
	private String numeroCuenta;

	@NotNull(message = "Le moneda de la cuenta corriente no puede ser nula.")
	private Long monedaId;

	public Long getMonedaId() {
		return monedaId;
	}

	public void setMonedaId(Long monedaId) {
		this.monedaId = monedaId;
	}

	public String getNumeroCuenta() {
		return numeroCuenta;
	}

	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}

}
