package coop.tecso.examen.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class CuentaCorrienteDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String numeroCuenta;
	private MonedaDto moneda;
	private BigDecimal saldo;

	public String getNumeroCuenta() {
		return numeroCuenta;
	}

	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public MonedaDto getMoneda() {
		return moneda;
	}

	public void setMoneda(MonedaDto moneda) {
		this.moneda = moneda;
	}

	public BigDecimal getSaldo() {
		return saldo;
	}

	public void setSaldo(BigDecimal saldo) {
		this.saldo = saldo;
	}

}
