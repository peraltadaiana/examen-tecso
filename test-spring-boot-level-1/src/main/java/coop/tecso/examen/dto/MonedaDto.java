package coop.tecso.examen.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class MonedaDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String descripcion;
	private String codigo;
	private BigDecimal montoMaximoDescubierto;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public BigDecimal getMontoMaximoDescubierto() {
		return montoMaximoDescubierto;
	}

	public void setMontoMaximoDescubierto(BigDecimal montoMaximoDescubierto) {
		this.montoMaximoDescubierto = montoMaximoDescubierto;
	}

}
