package coop.tecso.examen.dto;

import java.io.Serializable;

public class TipoMovimientoDto implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long id;
	private String descripcion;
	private Integer signo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getSigno() {
		return signo;
	}

	public void setSigno(Integer signo) {
		this.signo = signo;
	}

}
