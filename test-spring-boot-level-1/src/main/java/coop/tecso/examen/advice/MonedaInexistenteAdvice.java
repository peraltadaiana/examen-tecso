package coop.tecso.examen.advice;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import coop.tecso.examen.exception.MonedaInexistenteException;

@ControllerAdvice
public class MonedaInexistenteAdvice {
	@ResponseBody
	@ExceptionHandler(MonedaInexistenteException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	String monedaInexistente(MonedaInexistenteException ex) {
		return ex.getMessage();
	}

}
