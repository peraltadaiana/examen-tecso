package coop.tecso.examen.advice;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import coop.tecso.examen.exception.CuentaCorrienteInexistenteException;

@ControllerAdvice
public class CuentaCorrienteInexistenteAdvice {
	@ResponseBody
	@ExceptionHandler(CuentaCorrienteInexistenteException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	String monedaInexistente(CuentaCorrienteInexistenteException ex) {
		return ex.getMessage();
	}

}
