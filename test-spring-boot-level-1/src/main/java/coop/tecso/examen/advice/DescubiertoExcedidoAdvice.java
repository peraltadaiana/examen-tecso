package coop.tecso.examen.advice;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;

import coop.tecso.examen.exception.DescubiertoExcedidoException;

@ControllerAdvice
public class DescubiertoExcedidoAdvice {
	@ResponseBody
	@ExceptionHandler(DescubiertoExcedidoException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	String descubiertoExedido(DescubiertoExcedidoException ex) {
		return ex.getMessage();
	}
}
