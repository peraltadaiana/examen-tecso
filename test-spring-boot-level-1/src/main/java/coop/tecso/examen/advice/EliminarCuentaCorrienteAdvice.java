package coop.tecso.examen.advice;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import coop.tecso.examen.exception.EliminarCuentaCorrienteException;

@ControllerAdvice
public class EliminarCuentaCorrienteAdvice {
	@ResponseBody
	@ExceptionHandler(EliminarCuentaCorrienteException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	String eliminarCuentaCorriente(EliminarCuentaCorrienteException ex) {
		return ex.getMessage();
	}

}
