package coop.tecso.examen.advice;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import coop.tecso.examen.exception.CuentaCorrienteDuplicadaException;

@ControllerAdvice
public class CuentaCorrienteDuplicadaAdvice {
	@ResponseBody
	@ExceptionHandler(CuentaCorrienteDuplicadaException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	String descubiertoExedido(CuentaCorrienteDuplicadaException ex) {
		return ex.getMessage();
	}
}
