package coop.tecso.examen.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import coop.tecso.examen.dto.MonedaDto;
import coop.tecso.examen.service.MonedaService;

@RestController
@RequestMapping("/moneda")
public class MonedaController {

	@Autowired
	private MonedaService monedaService;

	// Get all moneda
	@GetMapping
	public List<MonedaDto> getAllMoneda() {
		return monedaService.getAllMoneda();

	}

}
