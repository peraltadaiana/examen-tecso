package coop.tecso.examen.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import coop.tecso.examen.dto.CuentaCorrienteDto;
import coop.tecso.examen.dto.CuentaCorrienteInputDto;
import coop.tecso.examen.dto.MovimientoDto;
import coop.tecso.examen.dto.MovimientoInputDto;
import coop.tecso.examen.exception.CuentaCorrienteDuplicadaException;
import coop.tecso.examen.exception.CuentaCorrienteInexistenteException;
import coop.tecso.examen.exception.DescubiertoExcedidoException;
import coop.tecso.examen.exception.EliminarCuentaCorrienteException;
import coop.tecso.examen.exception.MonedaInexistenteException;

import coop.tecso.examen.service.CuentaCorrienteService;

@RestController
@RequestMapping("/cuentacorriente")
public class CuentaCorrienteController {

	@Autowired
	private CuentaCorrienteService cuentaCorrienteService;

	// Get All Cuenta Corriente
	@GetMapping
	public List<CuentaCorrienteDto> getAllCuentaCorriente() {		
		return cuentaCorrienteService.getAllCuentaCorriente();
	}

	// Get Cuenta Corriente
	@GetMapping("/{id}")
	public CuentaCorrienteDto getCuentaCorriente(@PathVariable("id") Long id)
			throws CuentaCorrienteInexistenteException {
		return cuentaCorrienteService.getCuentaCorriente(id);
	}

	// Get movimientos de una Cuenta Corriente
	@GetMapping("/{id}/movimientos")
	public List<MovimientoDto> getMovimientosCuentaCorriente(@PathVariable("id") Long id)
			throws CuentaCorrienteInexistenteException {
		return cuentaCorrienteService.getMovimientosCuentaCorriente(id);
	}

	// POST crear movimiento
	@PostMapping
	public void agregarCuentaCorriente(@Valid @RequestBody CuentaCorrienteInputDto dto)
			throws CuentaCorrienteDuplicadaException, MonedaInexistenteException {
		cuentaCorrienteService.agregarCuentaCorriente(dto);
		;
	}

	// POST crear movimiento
	@PostMapping("/{id}/movimiento")
	public void agregarMovimiento(@Valid @RequestBody MovimientoInputDto dto, @PathVariable("id") Long id)
			throws DescubiertoExcedidoException, CuentaCorrienteInexistenteException {
		cuentaCorrienteService.agregarMovimiento(dto, id);
	}

	// DELETE cuenta corriente
	@DeleteMapping("/{id}")
	public void eliminarCuentaCorriente(@PathVariable("id") Long id)
			throws EliminarCuentaCorrienteException, CuentaCorrienteInexistenteException {
		cuentaCorrienteService.eliminarCuentaCorriente(id);
	}

}
