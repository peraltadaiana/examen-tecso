package coop.tecso.examen.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import coop.tecso.examen.dto.TipoMovimientoDto;
import coop.tecso.examen.service.TipoMovimientoService;

@RestController
@RequestMapping("/tipomovimiento")
public class TipoMovimientoController {

	@Autowired
	private TipoMovimientoService tipoMovimientoService;

	// Get All TipoMovimiento
	@GetMapping
	public List<TipoMovimientoDto> getAllTipoMovimiento() {
		return tipoMovimientoService.getAllTipoMovimiento();

	}

}
