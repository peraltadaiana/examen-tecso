package coop.tecso.examen.exception;

public class EliminarCuentaCorrienteException extends Exception{

	private static final long serialVersionUID = 1L;
	
	public EliminarCuentaCorrienteException(String numeroCuenta) {
        super("La cuenta corriente " + numeroCuenta + " No puede eliminarse, posee movimientos.");
    }
}
