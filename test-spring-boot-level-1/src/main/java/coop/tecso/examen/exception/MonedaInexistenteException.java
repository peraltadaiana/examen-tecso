package coop.tecso.examen.exception;

public class MonedaInexistenteException extends Exception {

	private static final long serialVersionUID = 1L;

	public MonedaInexistenteException() {
		super("La moneda es requerida.");
	}

}
