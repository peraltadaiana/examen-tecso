package coop.tecso.examen.exception;

public class CuentaCorrienteDuplicadaException extends Exception {

	private static final long serialVersionUID = 1L;

	public CuentaCorrienteDuplicadaException(String numeroCuenta) {
		super("El número de cuenta que desea agregar (" + numeroCuenta + "), ya existe.");
	}

}
