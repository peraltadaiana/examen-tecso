package coop.tecso.examen.exception;

public class CuentaCorrienteInexistenteException extends Exception {

	private static final long serialVersionUID = 1L;

	public CuentaCorrienteInexistenteException() {
		super("La Cuenta Corriente no existe.");
	}

}
