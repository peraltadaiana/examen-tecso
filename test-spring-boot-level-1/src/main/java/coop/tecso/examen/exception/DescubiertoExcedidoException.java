package coop.tecso.examen.exception;

import java.math.BigDecimal;
import java.text.NumberFormat;

public class DescubiertoExcedidoException extends Exception {

	private static final long serialVersionUID = 1L;

	public DescubiertoExcedidoException(String numeroCuenta, BigDecimal saldo, BigDecimal decubierto) {
		super("La cuenta " + numeroCuenta + " excede el descubierto. (Descubierto permitido: " + decubierto
				+ ". Descubierto generado: " + NumberFormat.getCurrencyInstance().format(saldo) + ").");
	}

}