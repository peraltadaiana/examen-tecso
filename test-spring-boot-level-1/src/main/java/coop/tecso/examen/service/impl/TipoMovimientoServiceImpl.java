package coop.tecso.examen.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import coop.tecso.examen.dto.TipoMovimientoDto;
import coop.tecso.examen.model.TipoMovimiento;
import coop.tecso.examen.repository.TipoMovimientoRepository;
import coop.tecso.examen.service.TipoMovimientoService;

@Service
public class TipoMovimientoServiceImpl implements TipoMovimientoService {

	@Autowired
	private TipoMovimientoRepository tipoMovimientoRepository;

	@Override
	public List<TipoMovimientoDto> getAllTipoMovimiento() {
		List<TipoMovimientoDto> result = new ArrayList<>();
		for (TipoMovimiento entity : tipoMovimientoRepository.findAll()) {
			TipoMovimientoDto dto = new TipoMovimientoDto();
			dto.setId(entity.getId());
			dto.setDescripcion(entity.getDescripcion());
			dto.setSigno(entity.getSigno());
			result.add(dto);
		}

		return result;
	}

}
