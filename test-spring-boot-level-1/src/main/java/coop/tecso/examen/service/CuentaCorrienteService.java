package coop.tecso.examen.service;

import coop.tecso.examen.dto.MovimientoInputDto;
import coop.tecso.examen.exception.CuentaCorrienteDuplicadaException;
import coop.tecso.examen.exception.CuentaCorrienteInexistenteException;
import coop.tecso.examen.exception.DescubiertoExcedidoException;
import coop.tecso.examen.exception.EliminarCuentaCorrienteException;
import coop.tecso.examen.exception.MonedaInexistenteException;

import java.util.List;

import coop.tecso.examen.dto.CuentaCorrienteDto;
import coop.tecso.examen.dto.CuentaCorrienteInputDto;
import coop.tecso.examen.dto.MovimientoDto;

public interface CuentaCorrienteService {

	public void agregarMovimiento(MovimientoInputDto dto, Long cuentaCorrienteId)
			throws DescubiertoExcedidoException, CuentaCorrienteInexistenteException;

	public List<CuentaCorrienteDto> getAllCuentaCorriente();

	public CuentaCorrienteDto getCuentaCorriente(Long id) throws CuentaCorrienteInexistenteException;

	public List<MovimientoDto> getMovimientosCuentaCorriente(Long id) throws CuentaCorrienteInexistenteException;

	public void agregarCuentaCorriente(CuentaCorrienteInputDto dto)
			throws CuentaCorrienteDuplicadaException, MonedaInexistenteException;

	public void eliminarCuentaCorriente(Long cuentaCorrienteId)
			throws EliminarCuentaCorrienteException, CuentaCorrienteInexistenteException;
}
