package coop.tecso.examen.service;

import java.util.List;

import coop.tecso.examen.dto.MonedaDto;

public interface MonedaService {

	public List<MonedaDto> getAllMoneda();
}
