package coop.tecso.examen.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import coop.tecso.examen.dto.MonedaDto;
import coop.tecso.examen.model.Moneda;
import coop.tecso.examen.repository.MonedaRepository;
import coop.tecso.examen.service.MonedaService;

@Service
public class MonedaServiceImpl implements MonedaService {
	
	@Autowired
	private MonedaRepository monedaRepository;

	@Override
	public List<MonedaDto> getAllMoneda() {
		
		List<MonedaDto> result = new ArrayList<>();
		for (Moneda entity : monedaRepository.findAll()) {
			MonedaDto dto = new MonedaDto();
			dto.setId(entity.getId());
			dto.setCodigo(entity.getCodigo());
			dto.setDescripcion(entity.getDescripcion());
			dto.setMontoMaximoDescubierto(entity.getMontoMaximoDescubierto());
					
			result.add(dto);
		}
		
	    return result;
	}

}
