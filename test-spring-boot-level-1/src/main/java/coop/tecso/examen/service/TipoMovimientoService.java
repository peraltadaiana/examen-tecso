package coop.tecso.examen.service;

import java.util.List;

import coop.tecso.examen.dto.TipoMovimientoDto;

public interface TipoMovimientoService {

	public List<TipoMovimientoDto> getAllTipoMovimiento();

}
