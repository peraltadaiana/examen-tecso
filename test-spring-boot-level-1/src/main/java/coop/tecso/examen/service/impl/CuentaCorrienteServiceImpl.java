package coop.tecso.examen.service.impl;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;

import coop.tecso.examen.dto.CuentaCorrienteDto;
import coop.tecso.examen.dto.CuentaCorrienteInputDto;
import coop.tecso.examen.dto.MonedaDto;
import coop.tecso.examen.dto.MovimientoDto;
import coop.tecso.examen.dto.MovimientoInputDto;
import coop.tecso.examen.exception.CuentaCorrienteDuplicadaException;
import coop.tecso.examen.exception.CuentaCorrienteInexistenteException;
import coop.tecso.examen.exception.DescubiertoExcedidoException;
import coop.tecso.examen.exception.EliminarCuentaCorrienteException;
import coop.tecso.examen.exception.MonedaInexistenteException;
import coop.tecso.examen.model.CuentaCorriente;
import coop.tecso.examen.model.Moneda;
import coop.tecso.examen.model.Movimiento;
import coop.tecso.examen.repository.CuentaCorrienteRepository;
import coop.tecso.examen.repository.MonedaRepository;

import coop.tecso.examen.repository.TipoMovimientoRepository;
import coop.tecso.examen.service.CuentaCorrienteService;

@Service
public class CuentaCorrienteServiceImpl implements CuentaCorrienteService {

	@Autowired
	private CuentaCorrienteRepository cuentaCorrienteRepository;

	@Autowired
	private TipoMovimientoRepository tipoMovimientoRepository;

	@Autowired
	private MonedaRepository monedaRepository;

	@Override
	public void agregarMovimiento(MovimientoInputDto dto, Long cuentaCorrienteId)
			throws DescubiertoExcedidoException, CuentaCorrienteInexistenteException {
		Optional<CuentaCorriente> entity = cuentaCorrienteRepository.findById(cuentaCorrienteId);
		if (!entity.isPresent())
			throw new CuentaCorrienteInexistenteException();
		CuentaCorriente cuentaCorriente = entity.get();

		cuentaCorriente.agregarMovimiento(dto, tipoMovimientoRepository.getOne(dto.getTipoMovimientoId()));
		cuentaCorrienteRepository.save(cuentaCorriente);
	}

	@Override
	public void agregarCuentaCorriente(CuentaCorrienteInputDto dto)
			throws CuentaCorrienteDuplicadaException, MonedaInexistenteException {
		CuentaCorriente entity = new CuentaCorriente();
		Optional<Moneda> moneda = monedaRepository.findById(dto.getMonedaId());
		entity.setNumeroCuenta(dto.getNumeroCuenta().toUpperCase());
		// la moneda no puede ser nula
		if (!moneda.isPresent()) {
			throw new MonedaInexistenteException();
		} else {
			entity.setMoneda(moneda.get());
		}
		try {
			cuentaCorrienteRepository.save(entity);
		} catch (DataIntegrityViolationException e) {
			// Numero de cuenta corriente duplicado
			throw new CuentaCorrienteDuplicadaException(dto.getNumeroCuenta().toUpperCase());
		}

	}

	@Override
	public List<CuentaCorrienteDto> getAllCuentaCorriente() {
		List<CuentaCorrienteDto> result = new ArrayList<>();
		for (CuentaCorriente entity : cuentaCorrienteRepository.findAll()) {
			CuentaCorrienteDto dto = new CuentaCorrienteDto();
			dto.setId(entity.getId());
			MonedaDto moneda = new MonedaDto();
			moneda.setCodigo(entity.getMoneda().getCodigo());
			moneda.setDescripcion(entity.getMoneda().getDescripcion());
			moneda.setId(entity.getMoneda().getId());
			moneda.setMontoMaximoDescubierto(entity.getMoneda().getMontoMaximoDescubierto());
			dto.setMoneda(moneda);
			dto.setNumeroCuenta(entity.getNumeroCuenta());
			dto.setSaldo(entity.getSaldoBD());

			result.add(dto);
		}

		return result;
	}

	@Override
	public void eliminarCuentaCorriente(Long cuentaCorrienteId)
			throws EliminarCuentaCorrienteException, CuentaCorrienteInexistenteException {
		Optional<CuentaCorriente> entity = cuentaCorrienteRepository.findById(cuentaCorrienteId);
		if (!entity.isPresent())
			throw new CuentaCorrienteInexistenteException();

		CuentaCorriente cuentaCorriente = entity.get();

		if (cuentaCorriente.getMovimientoList().size() > 0) {
			throw new EliminarCuentaCorrienteException(cuentaCorriente.getNumeroCuenta());
		} else {
			cuentaCorrienteRepository.deleteById(cuentaCorrienteId);
		}

	}

	@Override
	public CuentaCorrienteDto getCuentaCorriente(Long id) throws CuentaCorrienteInexistenteException {
		Optional<CuentaCorriente> entity = cuentaCorrienteRepository.findById(id);
		if (!entity.isPresent())
			throw new CuentaCorrienteInexistenteException();

		CuentaCorriente cuentaCorriente = entity.get();

		CuentaCorrienteDto dto = new CuentaCorrienteDto();
		dto.setId(cuentaCorriente.getId());
		MonedaDto moneda = new MonedaDto();
		moneda.setCodigo(cuentaCorriente.getMoneda().getCodigo());
		moneda.setDescripcion(cuentaCorriente.getMoneda().getDescripcion());
		moneda.setId(cuentaCorriente.getMoneda().getId());
		moneda.setMontoMaximoDescubierto(cuentaCorriente.getMoneda().getMontoMaximoDescubierto());
		dto.setMoneda(moneda);
		dto.setNumeroCuenta(cuentaCorriente.getNumeroCuenta());
		dto.setSaldo(cuentaCorriente.getSaldoBD());
		return dto;
	}

	@Override
	public List<MovimientoDto> getMovimientosCuentaCorriente(Long id) throws CuentaCorrienteInexistenteException {
		Optional<CuentaCorriente> entity = cuentaCorrienteRepository.findById(id);
		if (!entity.isPresent())
			throw new CuentaCorrienteInexistenteException();

		CuentaCorriente cuentaCorriente = entity.get();

		List<MovimientoDto> result = new ArrayList<>();
		for (Movimiento entityMovimiento : cuentaCorriente.getMovimientoList()) {
			MovimientoDto dto = new MovimientoDto();
			dto.setId(entityMovimiento.getId());
			dto.setDescripcionTipoMovimiento(entityMovimiento.getTipoMovimiento().getDescripcion());
			dto.setFecha(entityMovimiento.getFecha());
			dto.setImporte(entityMovimiento.getImporte());
			dto.setDescripcion(entityMovimiento.getDescripcion());
			result.add(dto);
		}

		result.sort(new Comparator<MovimientoDto>() {

			@Override
			public int compare(MovimientoDto o1, MovimientoDto o2) {
				if (o1.getFecha().after(o2.getFecha()))
					return 1;
				else if (o1.getFecha().before(o2.getFecha()))
					return -1;
				else
					return 0;
			}
		});

		return Lists.reverse(result);
	}

}
