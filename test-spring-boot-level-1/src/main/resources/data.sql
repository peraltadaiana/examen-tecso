-- INICIO INSERT TIPO MOVIMIENTO---------------------------------------------------------------------
INSERT INTO tipo_movimiento (descripcion,signo, creation_timestamp, modification_timestamp, version_number) 
VALUES ('DEBITO',-1, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);

INSERT INTO tipo_movimiento (descripcion,signo, creation_timestamp, modification_timestamp, version_number) 
VALUES ('CREDITO',1, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);
-- FIN INSERT TIPO MOVIMIENTO---------------------------------------------------------------------

-- INICIO INSERT MONEDA---------------------------------------------------------------------
INSERT INTO moneda (descripcion,monto_maximo_descubierto,codigo, creation_timestamp, modification_timestamp, version_number) 
VALUES ('PESO ARGENTINO',1000,'ARS', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);

INSERT INTO moneda (descripcion,monto_maximo_descubierto,codigo, creation_timestamp, modification_timestamp, version_number) 
VALUES ('DOLAR ESTADOUNIDENSE',300,'USD', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);

INSERT INTO moneda (descripcion,monto_maximo_descubierto,codigo, creation_timestamp, modification_timestamp, version_number) 
VALUES ('EURO',150,'EUR', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);
-- FIN INSERT MONEDA---------------------------------------------------------------------


-- INICIO INSERT CUENTA CORRIENTE------------------------------------------------------------

INSERT INTO cuenta_corriente (moneda_id,numero_cuenta, creation_timestamp, modification_timestamp, version_number) 
VALUES (1,'CC045637281', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);

INSERT INTO cuenta_corriente (moneda_id,numero_cuenta, creation_timestamp, modification_timestamp, version_number) 
VALUES (2,'CC044323545', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);

-- FIN INSERT CUENTA CORRIENTE---------------------------------------------------------------


-- INICIO INSERT MOVIMIENTO---------------------------------------------------------------------

INSERT INTO movimiento (fecha,tipo_movimiento_id,cuenta_corriente_id,descripcion,importe, creation_timestamp, modification_timestamp, version_number) 
VALUES ('2021-01-01 09:41:05.0+00',1,1,'Transferencia bancaria 1',10000.54, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);

INSERT INTO movimiento (fecha,tipo_movimiento_id,cuenta_corriente_id,descripcion,importe, creation_timestamp, modification_timestamp, version_number) 
VALUES ('2021-01-02 11:41:05.0+00',1,1,'Transferencia bancaria 2',100000.54, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);

INSERT INTO movimiento (fecha,tipo_movimiento_id,cuenta_corriente_id,descripcion,importe, creation_timestamp, modification_timestamp, version_number) 
VALUES ('2021-01-04 08:41:05.0+00',2,1,'Acreditacion de Haberes Enero',110000, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);

INSERT INTO movimiento (fecha,tipo_movimiento_id,cuenta_corriente_id,descripcion,importe, creation_timestamp, modification_timestamp, version_number) 
VALUES ('2021-01-04 13:41:05.0+00',2,1,'Acreditacion de Haberes Febrero',120000, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);


-- FIN INSERT MOVIMIENTO---------------------------------------------------------------------


