package coop.tecso.examen.repository;

import static org.junit.Assert.assertTrue;


import java.math.BigDecimal;
import java.util.HashSet;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringRunner;

import coop.tecso.examen.model.CuentaCorriente;
import coop.tecso.examen.model.Moneda;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CuentaCorrienteRepositoryTest {

	@Autowired
	private CuentaCorrienteRepository cuentaCorrienteRepository;

	@Autowired
	private MonedaRepository monedaRepository;

	private Moneda moneda;

	@Before
	public void setUp() {
		this.moneda = new Moneda();
		moneda.setCodigo("UN_CODIGO");
		moneda.setDescripcion("UNA_DESCRIPCION");
		moneda.setMontoMaximoDescubierto(new BigDecimal(123));

		monedaRepository.save(moneda);
	}

	/**
	 * Numero de cuenta obligatorio
	 * 
	 * @throws Exception
	 */
	@Test
	public void guardarCuentaCorrienteSinNumero() throws Exception {
		CuentaCorriente cuentaCorriente = new CuentaCorriente();
		cuentaCorriente.setMoneda(moneda);
		cuentaCorriente.setMovimientoList(new HashSet<>());
		try {
			cuentaCorrienteRepository.save(cuentaCorriente);
		} catch (DataIntegrityViolationException e) {
			assertTrue("Numero de cuenta obligatorio", true);
		}
	}

	/**
	 * Numero de cuenta repetido
	 * 
	 * @throws Exception
	 */
	@Test
	public void validarNumeroDeCuentaRepeito() throws Exception {
		CuentaCorriente cuentaCorriente = new CuentaCorriente();
		cuentaCorriente.setMoneda(moneda);
		cuentaCorriente.setNumeroCuenta("UN_NUMERO");
		cuentaCorriente.setMovimientoList(new HashSet<>());

		CuentaCorriente cuentaCorrienteDuplicada = new CuentaCorriente();
		cuentaCorrienteDuplicada.setMoneda(moneda);
		cuentaCorrienteDuplicada.setNumeroCuenta("UN_NUMERO");
		cuentaCorrienteDuplicada.setMovimientoList(new HashSet<>());

		cuentaCorrienteRepository.save(cuentaCorriente);

		try {
			cuentaCorrienteRepository.save(cuentaCorrienteDuplicada);

		} catch (DataIntegrityViolationException e) {
			assertTrue("Numero de cuenta duplicado", true);
		}
	}

	/**
	 * Valida que se puedan guardar dos cuentas con distintos numeros.
	 * @throws Exception
	 */
	@Test
	public void validarNumeroDeCuentaDistinto() throws Exception {
		CuentaCorriente cuentaCorriente = new CuentaCorriente();
		cuentaCorriente.setMoneda(moneda);
		cuentaCorriente.setNumeroCuenta("UN_NUMERO");
		cuentaCorriente.setMovimientoList(new HashSet<>());

		CuentaCorriente cuentaCorrienteDistinto = new CuentaCorriente();
		cuentaCorrienteDistinto.setMoneda(moneda);
		cuentaCorrienteDistinto.setNumeroCuenta("OTRO_NUMERO");
		cuentaCorrienteDistinto.setMovimientoList(new HashSet<>());

		cuentaCorrienteRepository.save(cuentaCorriente);

		try {
			cuentaCorrienteRepository.save(cuentaCorrienteDistinto);

		} catch (DataIntegrityViolationException e) {
			assertTrue("Numero de cuenta duplicado", true);
		}
	}
}
