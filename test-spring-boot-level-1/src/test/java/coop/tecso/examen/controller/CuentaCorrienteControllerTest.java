package coop.tecso.examen.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.math.BigDecimal;

import java.util.Date;
import java.util.HashSet;
import java.util.Optional;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import org.springframework.web.bind.annotation.RequestMapping;

import coop.tecso.examen.dto.MovimientoInputDto;
import coop.tecso.examen.model.CuentaCorriente;
import coop.tecso.examen.model.Moneda;
import coop.tecso.examen.model.Movimiento;
import coop.tecso.examen.model.TipoMovimiento;
import coop.tecso.examen.repository.CuentaCorrienteRepository;
import coop.tecso.examen.repository.MonedaRepository;
import coop.tecso.examen.repository.TipoMovimientoRepository;
import coop.tecso.examen.service.CuentaCorrienteService;
import coop.tecso.examen.service.impl.CuentaCorrienteServiceImpl;

@RunWith(SpringRunner.class)
@WebMvcTest(CuentaCorrienteController.class)
public class CuentaCorrienteControllerTest {

	@TestConfiguration
	static class CuentaCorrienteServiceImplTestContextConfiguration {

		@Bean
		public CuentaCorrienteService cuentaCorrienteService() {
			return new CuentaCorrienteServiceImpl();
		}
	}

	private final BigDecimal MONTO_MAX_DESCUBIERTO = new BigDecimal(999d);

	@Before
	public void setUp() {
		mockMoneda = new Moneda();
		mockMoneda.setCodigo("TT");
		mockMoneda.setDescripcion("MONEDA TEST");
		mockMoneda.setMontoMaximoDescubierto(MONTO_MAX_DESCUBIERTO);
		monedaRepository.save(mockMoneda);

		mockTipoMovimiento = new TipoMovimiento();
		mockTipoMovimiento.setDescripcion("TEST TIPO MOVIMIENTO");
		mockTipoMovimiento.setSigno(1);

		tipoMovimientoRepository.save(mockTipoMovimiento);
	}

	private Moneda mockMoneda;
	private TipoMovimiento mockTipoMovimiento;

	@Autowired
	private MockMvc mvc;

	@Autowired
	private CuentaCorrienteController controller;

	@MockBean
	private CuentaCorrienteRepository cuentaCorrienteRepository;

	@MockBean
	private MonedaRepository monedaRepository;

	@MockBean
	private TipoMovimientoRepository tipoMovimientoRepository;

	@Test
	public void deleteSinMovimientoReturnsOk() throws Exception {
		CuentaCorriente cuentaCorriente = new CuentaCorriente();
		cuentaCorriente.setMoneda(mockMoneda);
		cuentaCorriente.setNumeroCuenta("UNNUMERO");
		cuentaCorriente.setId(1L);
		cuentaCorriente.setMovimientoList(new HashSet<>());

		when(cuentaCorrienteRepository.findById(1L)).thenReturn(Optional.of(cuentaCorriente));

		String root = controller.getClass().getAnnotation(RequestMapping.class).value()[0];

		mvc.perform(delete(root + "/" + cuentaCorriente.getId())).andDo(print()).andExpect(status().isOk()).andReturn();
	}

	@Test
	public void deleteConMovimientoReturnsBadRequest() throws Exception {
		CuentaCorriente cuentaCorriente = new CuentaCorriente();
		cuentaCorriente.setMoneda(mockMoneda);
		cuentaCorriente.setNumeroCuenta("UNNUMERO");
		cuentaCorriente.setId(1L);
		cuentaCorriente.setMovimientoList(new HashSet<>());

		Movimiento movimiento = new Movimiento();
		movimiento.setCuentaCorriente(cuentaCorriente);
		movimiento.setDescripcion("TEST MOV");
		movimiento.setFecha(new Date());
		movimiento.setId(1L);
		movimiento.setImporte(new BigDecimal(1000d));
		movimiento.setTipoMovimiento(mockTipoMovimiento);

		cuentaCorriente.getMovimientoList().add(movimiento);

		when(cuentaCorrienteRepository.findById(1L)).thenReturn(Optional.of(cuentaCorriente));

		String root = controller.getClass().getAnnotation(RequestMapping.class).value()[0];

		mvc.perform(delete(root + "/" + cuentaCorriente.getId())).andDo(print()).andExpect(status().isBadRequest())
				.andReturn();
	}

	/**
	 * Valida que si paso una descripcion mayor a 200 caracteres arroja una
	 * excepcion
	 * 
	 * @throws Exception
	 */

	@Test
	public void postDescripcionLarga() throws Exception {

		MovimientoInputDto movimientoInputDto = new MovimientoInputDto();

		int longitud = 201;
		String cadena = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		StringBuilder cadenaAleatoria = new StringBuilder();
		Random rnd = new Random();

		for (int i = 0; i < longitud; i++) {
			int indice = rnd.nextInt(cadena.length());
			cadenaAleatoria.append(cadena.charAt(indice));
		}

		movimientoInputDto.setDescripcion(cadenaAleatoria.toString());
		movimientoInputDto.setImporte(new BigDecimal(123));
		movimientoInputDto.setTipoMovimientoId(2l);

		String root = controller.getClass().getAnnotation(RequestMapping.class).value()[0];

		mvc.perform(post(root + "/1/movimiento", movimientoInputDto)).andDo(print()).andExpect(status().isBadRequest());
	}

	
}
